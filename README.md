# gdpr-template

## References

* https://www.cnil.fr/fr/agir
    * https://www.cnil.fr/en/data-protection-around-the-world
* https://www.datarequests.org/blog
    * https://www.datarequests.org/blog/sample-letter-gdpr-erasure-request/
    * https://www.datarequests.org/blog/gdpr-cheat-sheet/
    * https://www.datarequests.org/blog/gdpr-territorial-scope/
    * https://www.datarequests.org/blog/your-gdpr-rights/

## Français

Ce projet vous permet d'accéder à un exemple de demande formelle de suppression de compte (quel que
soit le compte) conformément à la loi française sur la protection des données ainsi qu'au règlement
général européen sur la protection des données (RGPD). Cet exemple se veut le plus juridiquement
valide possible, mais cela dit, n'étant pas juriste : je vous recommande tout de même de consulter
la [CNIL](https://www.cnil.fr/fr/agir) (autorité compétente sur le sujet, en France).

## English

This project enables you to access an example of a formal request for account deletion (for any
account) in accordance with the European General Data Protection Regulation (GDPR). This example is
meant to be as legally valid as possible, but that being said, and since I am not a lawyer: I
recommend you to consult the competent authority on the subject of [your
country](https://www.cnil.fr/en/data-protection-around-the-world).

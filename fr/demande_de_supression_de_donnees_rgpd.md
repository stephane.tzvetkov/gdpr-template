<!-- Markdown généré avec la commande suivante `$ pandoc -s exemple.tex -o exemple.md` -->

\[adresse\], \[code postal\], \[ville\]
\
\[date\]

\[Organisme\]

Madame, Monsieur,

Disposant actuellement d'un compte chez \[Organisme\], je vous fais part
de ma volonté de supprimer complètement mon compte (pas seulement de le
fermer) et d'effacer **toutes** mes données personnelles associées,
conformément à la loi française sur la protection des données et au
règlement général européen sur la protection des données (RGPD).

Je souhaite, en particulier, vous souligner mon attachement au respect
des points suivants :

-   Je demande l'effacement immédiat des données me concernant,
    conformément à l'article 15 du RGPD et tel que défini à l'article
    4(1) du RGPD.

-   Je suis d'avis que les conditions énoncées à l'article 17(1) du RGPD
    sont remplies, c'est-à-dire que j'estime que les données réunies à
    mon sujet ne sont plus nécessaires aux fins pour lesquelles elles
    ont été collectées.

-   Si j'ai donné mon consentement au traitement des données me
    concernant (par exemple, conformément à l'article 6(1), ou à
    l'article 9(2), du RGPD), je retire ce consentement.

-   Je m'oppose au traitement des données me concernant (profilage
    inclus), conformément à l'article 21 du RGPD.

-   Si vous avez communiqué des données me concernant à des tiers, vous
    devez communiquer ma demande d'effacement, ainsi que toute référence
    à celles-ci, à chaque destinataire, conformément à l'article 19 du
    RGPD. Toujours conformément à l'article 19, je demande à être
    informé au sujet de ces destinataires.

-   Ma demande de suppression de données inclut explicitement tous les
    autres services et sociétés dont vous êtes responsable, au sens de
    l'article 4(7) du RGPD.

-   Conformément à l'article 12(3) du RGPD, vous devez me confirmer
    l'effacement dans un délai d'un mois à compter de la réception de
    cette demande. Si justifié, ce délai peut être prolongé de deux
    mois.

-   Je souhaite, comme décrit dans l'article 12(1), que le responsable
    du traitement me communique, au titre des articles 13 à 22 et de
    l'article 34, comment il a traité mes données selon ma demande.

Si vous vous opposez à l'effacement demandé, vous devez me le justifier.

J'inclus les informations suivantes nécessaires pour m'identifier :

-   NOM Prénom : \[NOM\] \[Prénom\]

-   Adesse : \[adresse\], \[ville\] \[code postal\]

-   Adresse électronique : \[adresse électronique\]

-   Photocopie de ma carte d'identité jointe à cette demande

Si vous avez besoin de plus d'informations pour m'identifier, merci de
me le faire savoir.

Si vous ne répondez pas à ma demande dans le délai imparti, je me
réserve le droit d'intenter une action en justice contre vous et
d'introduire une plainte auprès de l'autorité de contrôle compétente
(ici la Commission Nationale de l'Informatique et des Libertés - CNIL).

Je vous remercie pour l'attention que vous m'accordez et je vous prie de
croire en mes respectueuses salutations.\
 \
Pour faire et valoir ce que de droit.

\[Prénom\] \[Nom\] \[Signature\]
